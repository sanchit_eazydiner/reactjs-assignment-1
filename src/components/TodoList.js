import { constants } from "os";
import React from "react";
import TodoForm from "./TodoForm";
import Todo from "./todo";

export default class TodoList extends React.Component
{
    state = {
        todos: [],
        todoToShow: "all"
    };

    addTodo = todo => {
        this.setState({
           todos: [todo, ...this.state.todos]
        });
    };

    toggleComplete = id => {
        this.setState(state => ({
          todos: state.todos.map(todo => {
            if (todo.id === id) {
              return {
                ...todo,
                complete: !todo.complete
              };
            } else {
              return todo;
            }
          })
        }));
      };

    updateTodoToShow = s => {
        this.setState({
          todoToShow: s
        });
    };

    handleDeleteTodo = (id) => {
        this.setState({
            todos: this.state.todos.filter(todo => todo.id !== id)
        })
    }

    /*
    handleEditTodo = (todos) => {
        this.setState({
            ...todos,
        })
    }*/
    render()
    {
        /*
        let todos = [];

        if(this.state.todoToShow === "all")
        {
            todos = this.state.todos;
        }
        else if(this.state.todoToShow === "active")
        {
            todos = this.state.todos.filter(todo => !todo.complete);
        }
        else if(this.state.todoToShow === "complete")
        {
            todos = this.state.todos.filter(todo => todo.complete);
        }*/

        return (   
            <div>
                <TodoForm onSubmit={this.addTodo}/>
                <div style={{
                    fontFamily:"sans-serif",
                    fontSize:"20px",
                    color: "#fff"
                }}>Todo List :- </div>
                {this.state.todos.map(todo => (
                    <Todo 
                        key={todo.id} 
                        toggleComplete={() => this.toggleComplete(todo.id)} 
                        onDelete={() => this.handleDeleteTodo(todo.id)}
                        //onEdit={() => this.handleEditTodo(todo.id)}
                        todo={todo}
                    />
                ))}
                <div style={{color: "#fff",fontSize:"20px"}}>
                    Todos left: {this.state.todos.filter(todo => !todo.complete).length}
                </div>
                
            </div>
        );
    }
}

/*
<div>
                    <button onClick={() => this.updateTodoToShow("all")}>
                        all
                    </button>
                    <button onClick={() => this.updateTodoToShow("active")}>
                        active
                    </button>
                    <button onClick={() => this.updateTodoToShow("complete")}>
                        complete
                    </button>
                </div>

*/