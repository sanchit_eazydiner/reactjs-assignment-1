import React from "react";

export default props =>(
    <div style={{                        
    width: "50%",
    fontSize:"20px",
    padding: "12px 0px 12px 0px",
    margin: "8px",
    display: "inline-block",
    border: "4px solid #ccc",
    borderRadius: "4px",
    boxSizing: "border-box",
    backgroundColor: "#f2f2f2"}}>
    <div 
    style={{
        textDecoration: props.todo.complete ? "line-through" : ""
    }}
    onClick={props.toggleComplete}
    >
        {props.todo.text}
        
            <button style={{
                    fontFamily: "sans-serif",
                    fontSize: "15px",
                    backgroundColor: "#fc311e",
                    color: "#fff",
                    border: "0px",
                    borderRadius: "3px",
                    padding: "5px",
                    cursor: "pointer",
                    marginLeft: "35%",
                    marginRight: "5px"                
                }}
                onClick={props.onDelete}>Remove Item</button>
            <button style={{
                    fontFamily: "sans-serif",
                    fontSize: "15px",
                    backgroundColor: "#4682fa",
                    color: "#fff",
                    border: "0px",
                    borderRadius: "3px",
                    padding: "5px",
                    cursor: "pointer",
                    marginLeft: "5px",
                    marginRight: "5px"
                }} 
                onClick={props.onEdit}>Edit Item</button>
            </div>
    </div>
);