import React from 'react';
import shortid from 'shortid';

export default class TodoForm extends React.Component
{
    state = 
    {
        text: ""
    };

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };
    
    handleSubmit = (event) => {
        event.preventDefault();
        this.props.onSubmit({
            id:shortid.generate(),
            text: this.state.text,
            complete: false
        });
        this.setState({
            text: ""
        });
    };

    render()
    {
        return (
            <form onSubmit={this.handleSubmit}>
                <input
                    style={{
                        width: "70%",
                        padding: "12px 20px",
                        margin: "8px 0",
                        display: "inline-block",
                        border: "4px solid #ccc",
                        borderRadius: "4px",
                        boxSizing: "border-box",
                        backgroundColor: "#f2f2f2",
                        marginRight: "2.5%"
                    }}
                    name="text"   
                    value={this.state.text}
                    onChange={this.handleChange}
                    placeholder="todo..." 
                />
                <button style={{
                    fontFamily: "sans-serif",
                    fontSize: "18px",
                    backgroundColor: "#297",
                    color: "#fff",
                    border: "0px",
                    borderRadius: "3px",
                    padding: "15px",
                    cursor: "pointer",
                    marginBottom: "5px"                
                }} onClick={this.handleChange}>Add to List</button>
                
            </form>
            
        );
    }
}